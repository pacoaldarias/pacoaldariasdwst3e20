<?php
/**
 * Controles en formularios 7 - controles_formularios_7.php
 *
 * @author    Bartolomñ Sintes Marco <bartolome.sintes+mclibre@gmail.com>
 * @copyright 2012 Bartolomñ Sintes Marco
 * @license   http://www.gnu.org/licenses/agpl.txt AGPL 3 or later
 * @version   2012-10-23
 * @link      http://www.mclibre.org
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
print "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?" . ">\n";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Datos personales 7 (Resultado). Controles en formularios. Ejercicios.
            PHP. Bartolomñ Sintes Marco</title>
        <link href="mclibre_php_soluciones.css" rel="stylesheet" type="text/css"
              title="Color" />
    </head>

    <body>
        <h1>Datos personales 7 (Resultado)</h1>
        <?php

        function recoge($var) {
           $tmp = (isset($_REQUEST[$var])) ? strip_tags(trim(htmlspecialchars($_REQUEST[$var], ENT_QUOTES, "ISO-8859-1"))) : "";
           if (get_magic_quotes_gpc()) {
              $tmp = stripslashes($tmp);
           }
           return $tmp;
        }

        $nombre = recoge("nombre");
        $apellidos = recoge("apellidos");
        $edad = recoge("edad");
        $peso = recoge("peso");
        $sexo = recoge("sekso"); // El control no se llama sexo para evitar el proxy
        $estadoCivil = recoge("estadoCivil");
        $cine = recoge("cine");
        $deporte = recoge("deporte");
        $literatura = recoge("literatura");
        $musica = recoge("musica");
        $tebeos = recoge("tebeos");
        $television = recoge("television");

        if ($nombre == "") {
           print "<p class=\"aviso\">No ha escrito su nombre.</p>\n";
        } else {
           print "<p>Su nombre es <strong>$nombre</strong>.</p>\n";
        }

        if ($apellidos == "") {
           print "<p class=\"aviso\">No ha escrito sus apellidos.</p>\n";
        } else {
           print "<p>Sus apellidos son <strong>$apellidos</strong>.</p>\n";
        }

        if ($edad == 1) {
           print "<p>Tiene <strong>menos de 20 años</strong>.</p>\n";
        } elseif ($edad == 2) {
           print "<p>Tiene <strong>entre 20 y 39 años</strong>.</p>\n";
        } elseif ($edad == 3) {
           print "<p>Tiene <strong>entre 40 y 59 años</strong>.</p>\n";
        } elseif ($edad == 4) {
           print "<p>Tiene <strong>60 o mñs años</strong>.</p>\n";
        } else {
           print "<p class=\"aviso\">No ha indicado su edad.</p>\n";
        }

        if ($peso == "") {
           print "<p class=\"aviso\">No ha escrito su peso.</p>\n";
        } elseif (!is_numeric($peso)) {
           print "<p class=\"aviso\">No ha escrito su peso como nñmero.</p>\n";
        } elseif ($peso < 0) {
           print "<p class=\"aviso\">Ha ecrito un peso negativo.</p>\n";
        } else {
           print "<p>Su peso es de <strong>$peso</strong> Kg.</p>\n";
        }

        if ($sexo == "hombre") {
           print "<p>Es un <strong>hombre</strong>.</p>\n";
        } elseif ($sexo == "mujer") {
           print "<p>Es una <strong>mujer</strong>.</p>\n";
        } else {
           print "<p class=\"aviso\">No ha marcado su sexo.</p>\n";
        }

        if ($estadoCivil == "soltero") {
           print "<p>Su estado civil es <strong>soltero</strong>.</p>\n";
        } elseif ($estadoCivil == "casado") {
           print "<p>Su estado civil es <strong>casado</strong>.</p>\n";
        } elseif ($estadoCivil == "otro") {
           print "<p>Su estado civil no es <strong>ni soltero ni casado</strong>.</p>\n";
        } else {
           print "<p class=\"aviso\">No ha marcado su estado civil.</p>\n";
        }

        if ($cine != "on" && $deporte != "on" && $literatura != "on" &&
                $musica != "on" && $tebeos != "on" && $television != "on") {
           print "<p class=\"aviso\">No ha marcado ninguna aficiñn.</p>\n";
        } else {
           print "<p>Le gusta: \n";
           if ($cine == "on") {
              print "  <strong>el cine</strong>, \n";
           }
           if ($deporte == "on") {
              print "  <strong>el deporte</strong>, \n";
           }
           if ($literatura == "on") {
              print "  <strong>la literatura</strong>, \n";
           }
           if ($musica == "on") {
              print "  <strong>la mñsica</strong>, \n";
           }
           if ($tebeos == "on") {
              print "  <strong>los tebeos</strong>, \n";
           }
           if ($television == "on") {
              print "  <strong>la televisiñn</strong> \n";
           }
           print "</p>\n";
        }

        print "<p><a href=\"f7.html\">Volver al formulario.</a></p>\n";
        ?>



    </body>
</html>
