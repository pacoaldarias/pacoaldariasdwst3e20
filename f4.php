<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Datos personales 4 (Resultado). </title>
        <link href="Estilo.css" rel="stylesheet" type="text/css"
              title="Color" />
    </head>

    <body>
        <h1>Datos personales 4 (Resultado)</h1>
        <?php

        function recoge($var) {
           if (isset($_REQUEST[$var])) {
              $tmp = strip_tags(trim(htmlspecialchars($_REQUEST[$var])));
           } else {
              $tmp = "";
           };


           return $tmp;
        }

        $sexo = recoge("sekso"); // El control no se llama sexo para evitar el proxy

        if ($sexo == "hombre") {
           print "<p>Es un <strong>hombre</strong>.</p>\n";
        } elseif ($sexo == "mujer") {
           print "<p>Es una <strong>mujer</strong>.</p>\n";
        } else {
           print "<p class=\"aviso\">No ha marcado su sexo.</p>\n";
        }

        print "<p><a href=\"f4.html\">Volver al formulario.</a></p>\n";
        ?>

    </body>
</html>
