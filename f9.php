<html>
    <h1>Alta usuario (Resultado)</h1>
    <body>
        <?php

        function recoge($campo) {
           if (isset($_REQUEST[$campo])) {
              $valor = strip_tags(trim(htmlspecialchars($_REQUEST[$campo])));
           } else {
              $valor = "";
           };
           return $valor;
        }

        $nombre = recoge("nombre");
        $email = recoge("email");
        $patronEmail = "/^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})+$/";
        $user = recoge("user");
        $pass = recoge("pass");
        $pass2 = recoge("pass2");
        $bloqueado = recoge("bloqueado");
        $comercial = recoge("comercial");
        $marketing = recoge("marketing");
        $direccion = recoge("direccion");
        $fichero = "fichero.txt";

        if ($nombre == "") {
           print "<p class=\"aviso\">ERROR, debe introducir un nombre</p>\n";
        }
        if ($email == "") {
           print "<p class=\"aviso\">ERROR, debe introducir un email</p>\n";
        } else {
           if (!preg_match($patronEmail, $email)) {
              print("ERROR, el email no es válido");
           }
        }
        if ($user == "") {
           print "<p class=\"aviso\">ERROR, debe introducir un usuario</p>\n";
        }
        if ($pass == "" || $pass2 == "") {
           print "<p class=\"aviso\">ERROR, debe introducir la contraseña dos veces</p>\n";
        }
        if (!($pass == $pass2)) {
           print "<p class=\"aviso\">ERROR, la contraseña no coincide</p>\n";
        }
        if ($comercial == "" && $marketing == "" && $direccion == "") {
           print "<p class=\"aviso\">ERROR, debe marcar al menos un departamento</p>\n";
        }
        ?>

        <p>Alta correcta</p>

    </body>
</html>