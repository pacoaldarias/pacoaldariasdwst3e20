<html>
    <head>
        <title>DWS</title>
    </head>
    <body>

        <h1>Isset</h1>

        Isset devuelve cierto si existen un campo en el formulario con ese
        nombre.

        <form action="" method="get">
            <p>Nombre:<input type="text" name="nombre" size="20" maxlength="20" /></p>
            <p>
                <input type="submit" value="Enviar" />
                <input type="reset" value="Borrar" name="Reset" />
            </p>
        </form>

        <?php
        $campo = "nombre"; // poner nombre para que funcione bien
        if (isset($_REQUEST[$campo])) {
            $valor = $_REQUEST[$campo];
            if ($valor == "") {
                print "<p>El campo <strong>$campo</strong> está vacio  .</p>\n";
            } else {
                print "<p>Nombre: <strong>$campo: $valor </strong>.</p>\n";
            }
        }
        echo '<a href="isset_1.php">inicio</a>';
        ?>

</html>

