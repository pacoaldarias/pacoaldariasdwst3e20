<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <title>Datos personales 5 (Resultado). Controles en formularios. Ejercicios.
  PHP.</title>
  <link href="estilo.css" rel="stylesheet" type="text/css" title="Color" />
</head>

<body>
<h1>Datos personales 5 (Resultado)</h1>
<?php
function recoge($var)
{
    if (isset($_REQUEST[$var])){
        $tmp =strip_tags(trim(htmlspecialchars($_REQUEST[$var])));
    } else {
       $tmp = "";
    };

    
    return $tmp;
}

$cine       = recoge("cine");
$literatura = recoge("literatura");
$musica     = recoge("musica");

if ($cine == "on") {
    print "<p>Le gusta: <strong>el cine</strong></p>";
}
if ($literatura == "on") {
    print "<p>Le gusta: <strong>la literatura</strong></p>";
}
if ($musica == "on") {
    print "<p>Le gusta: <strong>la m�sica</strong></p>";
}
if ($cine != "on" && $literatura != "on" && $musica != "on") {
    print "<p class=\"aviso\">No ha marcado ninguna afici�n.</p>\n";
}

print "<p><a href=\"Ejer_Formularios5.html\">Volver al formulario.</a></p>\n";
?>

</body>
</html>
