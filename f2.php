<?php

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
       "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <title>Datos personales 2 (Resultado). Controles en formularios. Ejercicios. </title>
  <link href="estilo.css" rel="stylesheet" type="text/css"
  title="Color" />
</head>

<body>
<h1>Datos personales 2 (Resultado)</h1>
<?php

function recoge($var)
{
    $tmp = (isset($_REQUEST[$var]))
        ? strip_tags(trim(htmlspecialchars($_REQUEST[$var], ENT_QUOTES, "ISO-8859-1")))
        : "";
    if (get_magic_quotes_gpc()) {
        $tmp = stripslashes($tmp);
    }
    return $tmp;
}

$nombre = recoge("nombre");

if ($nombre == "") {
    print "<p class=\"aviso\">No ha escrito su nombre.</p>\n";
} else {
    print "<p>Su nombre es <strong>$nombre</strong>.</p>\n";
}

print "<p><a href=\"Ejer_Formularios2.html\">Volver al formulario.</a></p>\n";
?>

</body>
</html>
