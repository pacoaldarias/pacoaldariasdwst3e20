
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <title>Datos personales 3 (Resultado). Controles en formularios. Ejercicios.</title>
  <link href="estilo.css" rel="stylesheet" type="text/css" title="Color" />
</head>

<body>
<h1>Datos personales 3 (Resultado)</h1>
<?php
function recoge($var)
{
	//Tratamiento de condición reducido
    $tmp = (isset($_REQUEST[$var]))
        ? strip_tags(trim(htmlspecialchars($_REQUEST[$var], ENT_QUOTES, "ISO-8859-1")))
        : "";
   
    return $tmp;
}

$nombre    = recoge("nombre");
$apellidos = recoge("apellidos");

if ($nombre == "") {
    print "<p class=\"aviso\">No ha escrito su nombre.</p>\n";
} else {
    print "<p>Su nombre es <strong>$nombre</strong>.</p>\n";
}

if ($apellidos == "") {
    print "<p class=\"aviso\">No ha escrito sus apellidos.</p>\n";
} else {
    print "<p>Sus apellidos son <strong>$apellidos</strong>.</p>\n";
}

print "<p><a href=\"Ejer_Formularios3.html\">Volver al formulario.</a></p>\n";
?>

</body>
</html>
