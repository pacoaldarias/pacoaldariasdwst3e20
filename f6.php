
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Datos personales 6 (Resultado). Controles en formularios. Ejercicios. PHP.</title>
        <link href="estilo.css" rel="stylesheet" type="text/css" title="Color" />
    </head>

    <body>
        <h1>Datos personales 6 (Resultado)</h1>
        <?php

        function recoge($var) {
           $tmp = (isset($_REQUEST[$var])) ? strip_tags(trim(htmlspecialchars($_REQUEST[$var], ENT_QUOTES, "UTF-8"))) : "";
           return $tmp;
        }

        $edad = recoge("edad");

        if ($edad == 1) {
           print "<p>Tiene <strong>menos de 20 a�os</strong>.</p>\n";
        } elseif ($edad == 2) {
           print "<p>Tiene <strong>entre 20 y 39 a�os</strong>.</p>\n";
        } elseif ($edad == 3) {
           print "<p>Tiene <strong>entre 40 y 59 a�os</strong>.</p>\n";
        } elseif ($edad == 4) {
           print "<p>Tiene <strong>60 o m�s a�os</strong>.</p>\n";
        } else {
           print "<p class=\"aviso\">No ha indicado su edad.</p>\n";
        }

        print "<p><a href=\"f6.html\">Volver al formulario.</a></p>\n";
        ?>

    </body>
</html>
