<html>
    <head>
        <title>Isset</title>
    </head>
    <body>

        <h1>Isset</h1>

        Isset devuelve cierto si existen un campo en el formulario con ese
        nombre.

        <form action="" method="get">
            <p>Nombre:<input type="text" name="nombre" size="20" maxlength="20" /></p>
            <p>
                <input type="submit" value="Enviar" />
                <input type="reset" value="Borrar" name="Reset" />
            </p>
        </form>

        <?php
        $campo = "nombre"; // poner nombre para que funcione bien

        if (isset($_REQUEST[$campo])) {
            $nombre = $_REQUEST[$campo];
            if ($nombre == "") {

            } else
                print "<p>Su nombre es <strong>$nombre</strong>.</p>\n";
        }
        ?>

</html>

